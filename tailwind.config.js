/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      primary: '#f4e3cf',
      customred: '#953e45',
      reddark: '#481e21',
      smokewhite: '#fffdff',
      overlayColor: '#374151',
    }
  },
  plugins: [],
}