export default function Searchbar() {
    return (
        <div className="w-full">
            <form>   
                <label htmlFor="default-search" className="mb-2 text-sm font-medium text-customred sr-only dark:text-white">Search</label>
                <div className="relative">
                    <input type="search" id="default-search" className="block w-full px-4 py-2 text-sm text-customred border border-gray-300 bg-primary focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 placeholder:text-customred" placeholder="Find..." required>
                    </input>
                    <div className="absolute inset-y-0 right-0 flex items-center pr-3 cursor-pointer">
                        <svg className="w-4 h-4 text-customred dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                        </svg>
                    </div>
                </div>
            </form>
        </div>
    )
}