export default function NavbarMobile() {
  const menus = [
    "Videos",
    "People",
    "Documents",
    "Events",
    "Communities",
    "Favorites",
    "Channels",
  ];
  return (
    <div>
      <h5 className="text-md font-medium text-customred text-center pb-3 border-b border-opacity-10 border-customred dark:text-white">
        Main Menu
      </h5>
      <ul className="py-2 text-sm text-gray-700 dark:text-gray-200">
        {menus.map((item) => {
          return (
            <li key={item}>
              <a
                href="/"
                key={item}
                className="block px-2 py-2 text-sm text-customred font-semibold hover:bg-customred hover:bg-opacity-10 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
              >
                {item}
              </a>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
