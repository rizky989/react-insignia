export default function Navbar() {
    const menus = [
        "Videos",
        'People',
        'Documents',
        'Events',
        'Communities',
        'Favorites',
        'Channels'
    ]
  return (
    <nav
      className="flex py-3 my-5 text-primary border-y dark:bg-gray-800 dark:border-gray-700"
      aria-label="Breadcrumb"
    >
      <div className="inline-flex items-center justify-between">
        {menus.map((item, index) => {
          return (
            <span key={item} className="flex items-center">
              <span className={index === 0 ? 'hidden' : 'px-3'}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-slash"
                  viewBox="0 0 16 16"
                >
                  <path d="M11.354 4.646a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708l6-6a.5.5 0 0 1 .708 0z" />
                </svg>
              </span>
              <div>
                <a
                  href="/"
                  className="text-sm font-medium text-primary hover:dark:text-gray-400 dark:hover:text-white"
                >
                  {item}
                </a>
              </div>
            </span>
          );
        })}
      </div>
    </nav>
  );
}
