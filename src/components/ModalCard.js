import React from "react";

export default function ModalCard({ isOpen, onClose, content }) {
  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="fixed inset-0 bg-reddark opacity-60"></div>
      <div className="bg-primary w-full mx-2 py-3 lg:p-4 rounded-lg shadow-lg z-10">
        {content}
      </div>
      <button
        type="button"
        onClick={onClose}
        className="absolute bottom-24 text-smokewhite text-3xl font-bold"
      >
        &times;
      </button>
    </div>
  );
}
