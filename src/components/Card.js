export default function Card(props) {
  return (
    <div 
      className="block relative h-[130px] lg:min-h-[363px] p-3 bg-primary text-customred border border-gray-200 w-60 lg:w-full hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 bg-overlayColor bg-blend-multiply bg-opacity-30"
      style={{
        backgroundImage: `url(${props.data?.image})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      <div className="absolute bottom-3 flex">
        <div className="w-full lg:w-3/4 whitespace-pre-wrap">
          <h5 className="text-md lg:mb-2 font-bold tracking-tight text-gray-900">
            {props.data?.text}
          </h5>
          <p className="font-semibold text-2xs lg:text-xs text-gray-700 dark:text-gray-400 line-clamp-2">
            {props.data?.owner?.id}
          </p>
          <div className="block lg:hidden text-xs mt-2">
            {props.data?.likes} views
          </div>
        </div>
      </div>
      <div className="absolute hidden lg:block bottom-3 right-2 text-xs font-semibold">
            {props.data?.likes} views
      </div>
    </div>
  );
}
