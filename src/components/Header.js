import React, { useState } from "react";
import Searchbar from "./Searchbar";
import ModalCard from "./ModalCard";
import NavbarMobile from "./NavMobile";
import ProfileDropdown from "./ProfileDropdownButton";
export default function Header() {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav>
      {isOpen && (
        <div className="block lg:hidden">
          <ModalCard
            isOpen={isOpen}
            onClose={toggleMenu}
            content={<NavbarMobile />}
          />
        </div>
      )}
      <div className="max-w-screen-xl flex justify-between lg:items-center gap-4 mx-auto p-4">
        <button
          type="button"
          onClick={toggleMenu}
          className="inline-flex items-center w-5 h-5 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600 mt-0.5"
        >
          <span className="sr-only">Open main menu</span>
          <svg
            className="w-3 h-3"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 17 14"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M1 1h15M1 7h15M1 13h15"
            />
          </svg>
        </button>
        <a
          href="/"
          className="flex justify-normal lg:justify-center lg:w-fit w-full"
        >
          <span className="self-center text-md lg:text-4xl font-semibold whitespace-nowrap dark:text-white">
            Social
            <span className=" font-light">Network</span>
          </span>
        </a>
        <div className="flex-grow hidden lg:block w-full px-14">
          <Searchbar />
        </div>
        <div className="w-fit flex gap-4" id="navbar-default">
          <button
            type="button"
            className="text-primary lg:text-customred lg:bg-primary hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium text-sm py-1 lg:px-3 lg:py-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 flex"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-arrow-up"
              viewBox="0 0 16 16"
            >
              <path
                fillRule="evenodd"
                d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"
              />
            </svg>
            <span className="hidden lg:block ml-1">Upload</span>
          </button>

          <ProfileDropdown />
        </div>
      </div>
    </nav>
  );
}
