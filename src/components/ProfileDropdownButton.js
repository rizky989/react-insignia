import React, { useState } from "react";
import ProfileCard from "./ProfileCard";
import ModalCard from "./ModalCard";

const ProfileDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleProfile = () => {
    setIsOpen(!isOpen);
  };

  return (
    <button
      className="relative inline-flex text-customred p-0"
      onClick={toggleProfile} 
    >
      <div className="flex">
        <div className="text-primary lg:text-customred flex lg:bg-primary hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium text-sm py-1 lg:py-2.5 lg:px-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 mr-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-person"
            viewBox="0 0 16 16"
          >
            <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z" />
          </svg>
        </div>
        <div className="lg:text-primary text-left hidden lg:block">
          <div className="text-sm font-semibold">Arshad</div>
          <div className="text-xs font-light">Hassan</div>
        </div>
        <div className="absolute text-customred inline-flex items-center justify-center w-3 h-3 lg:w-5 lg:h-5 text-xs font-bold text-white bg-primary  rounded-full -top-0 -right-0 lg:-top-2 lg:-right-5 dark:border-gray-900">
          4
        </div>
      </div>
      {isOpen && (
        <div>
          <div className="absolute right-0 mt-12 w-56 bg-primary text-gray-800 rounded-lg h-auto hidden lg:block">
            <ProfileCard />
          </div>
          <div className="block lg:hidden">
            <ModalCard isOpen={isOpen} onClose={toggleProfile} content={<ProfileCard />}/>
          </div>
        </div>
      )}
    </button>
  );
};

export default ProfileDropdown;
