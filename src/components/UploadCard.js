export default function UploadCard() {
  return (
    <div className="block relative min-h-[175px] p-3 bg-customred text-primary border border-primary w-full hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
      <div className="h-full w-full align-middle flex items-center">
        <div className="flex justify-around gap-4 px-8 w-full">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="35"
            height="35"
            fill="currentColor"
            className="bi bi-upload text-2xl"
            viewBox="0 0 16 16"
          >
            <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
            <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z" />
          </svg>
          <h5 className="my-auto text-sm">
            <div className="font-bold">Upload</div>
            <div className="font-semibold">Your Own Video</div>
          </h5>
        </div>
      </div>
    </div>
  );
}
