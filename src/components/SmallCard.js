export default function SmallCard(props) {
  return (
    <div
      className="block relative min-h-[175px] p-3 bg-primary text-customred border border-gray-200 w-full hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 bg-overlayColor bg-blend-multiply bg-opacity-30"
      style={{
        backgroundImage: `url(${props.data?.image})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      <div className="absolute bottom-3 flex">
        <div className="w-3/4">
          <h5 className="mb-2 text-sm font-bold tracking-tight text-gray-900">
            {props.data?.text}
          </h5>
        </div>
      </div>
    </div>
  );
}
