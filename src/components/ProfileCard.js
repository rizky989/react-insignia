export default function ProfileCard() {
  return (
    <div>
      <div className="flex flex-col items-center pt-4 px-4">
        <div className="w-full flex-col flex items-center mx-auto">
          <img
            className="w-14 h-14 mb-3 rounded-lg -mt-16 lg:mt-0"
            src="http://placebeard.it/200/200"
            alt="Bonnie"
          />
          <h5 className="text-md font-medium text-gray-900 dark:text-white">
            Arshad Hassan
          </h5>
          <span className="text-xs text-gray-500 dark:text-gray-400">
            Visual Designer
          </span>
        </div>

        <div className="flex mt-4 w-full md:mt-2 border-b border-customred justify-around border-opacity-20 pb-3 mb-2">
          <a
            href="/"
            className="inline-flex gap-4 content-around justify-around place-content-around w-full px-4 py-2 text-sm font-medium text-center text-white bg-customred text-primary hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            <div className="flex gap-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-arrow-up"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"
                />
              </svg>
              Start Upload
            </div>
          </a>
        </div>
      </div>

      <div className="flex flex-col text-left">
        <ul aria-labelledby="dropdownButton">
          <li>
            <a
              href="/"
              className="block px-4 py-2 text-sm text-gray-700 hover:bg-customred hover:bg-opacity-10 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
            >
              My Profile
            </a>
          </li>
          <li>
            <a
              href="/"
              className="block px-4 py-2 text-sm text-gray-700 hover:bg-customred hover:bg-opacity-10 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
            >
              Edit Profile
            </a>
          </li>
          <li>
            <a
              href="/"
              className="block px-4 py-2 text-sm text-red-600 hover:bg-customred hover:bg-opacity-10 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
            >
              Security
            </a>
          </li>
        </ul>
      </div>

      <div className=" bg-customred bg-opacity-5 py-2 text-xs">
        <div className="flex align-center justify-center mx-auto text-center">
          Log Out{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-arrow-right-square-fill text-customred ml-2 rounded-full"
            viewBox="0 0 16 16"
          >
            <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z" />
          </svg>
        </div>
      </div>
    </div>
  );
}
