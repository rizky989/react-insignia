export default function Footer() {
  const social = ["twitter", "linkedin", "facebook"];
  const menus = [
    "About",
    "For Bussiness",
    "Suggestions",
    "Help & FAQs",
    "Contacts",
    "Pricing",
  ];
  const terms = ["Privacy", "Terms"];
  return (
    <div className="flex flex-col">
      <div
        className="flex py-3 mt-2 text-gray-700 border-t bg-gray-50 dark:bg-gray-800 dark:border-gray-700"
        aria-label="Breadcrumb"
      >
        <div className="inline-flex items-center justify-between mx-auto lg:mx-0">
          {social.map((item) => {
            return (
              <button
                key={item}
                type="button"
                className="text-customred flex bg-primary hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 text-sm py-2.5 px-3 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 mr-2"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-person"
                  viewBox="0 0 16 16"
                >
                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z" />
                </svg>
              </button>
            );
          })}
        </div>
      </div>

      <div className="flex flex-row items-center lg:flex-auto flex-wrap lg:justify-normal justify-center my-5">
        {menus.map((item, index) => {
          return (
            <span key={item} className="flex items-center">
              <span className={index === 0 ? "hidden" : "px-1"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-slash"
                  viewBox="0 0 16 16"
                >
                  <path d="M11.354 4.646a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708l6-6a.5.5 0 0 1 .708 0z" />
                </svg>
              </span>
              <div>
                <a
                  href="/"
                  className="text-sm text-gray-700 hover:text-blue-600 dark:text-gray-400 dark:hover:text-white"
                >
                  {item}
                </a>
              </div>
            </span>
          );
        })}
      </div>

      <div className="text-sm text-gray-700 text-center lg:text-left hover:text-blue-600 dark:text-gray-400 dark:hover:text-white">
        Copyright 2023 Companyname inc.
      </div>

      <div className="flex flex-row items-center lg:flex-auto flex-wrap lg:justify-normal justify-center my-5 pb-5">
        {terms.map((item, index) => {
          return (
            <span key={item} className="flex items-center">
              <span className={index === 0 ? "hidden" : "px-1"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-slash"
                  viewBox="0 0 16 16"
                >
                  <path d="M11.354 4.646a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708l6-6a.5.5 0 0 1 .708 0z" />
                </svg>
              </span>
              <div>
                <a
                  href="/"
                  className="text-sm text-gray-700 hover:text-blue-600 dark:text-gray-400 dark:hover:text-white"
                >
                  {item}
                </a>
              </div>
            </span>
          );
        })}
      </div>
    </div>
  );
}
