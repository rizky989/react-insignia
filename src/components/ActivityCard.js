export default function ActivityCard(props) {
  return (
    <div className="p-2.5 bg-white border-transparent bg-primary bg-opacity-10 rounded-sm align-middle dark:bg-gray-800 dark:border-gray-700 flex gap-3 overflow-hidden">
      <div 
        className="h-14 lg:w-20 w-16 lg:h-18 rounded-sm bg-primary my-auto"
        style={{
          backgroundImage: `url(${props.user.picture})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
      </div>
      <div className="w-full">
        <span className="mb-1 mr-1 text-sm lg:text-md font-semibold tracking-tight text-gray-900 dark:text-white">
          {props.user.firstName} {props.user.lastName}
        </span>
        <span className="font-light text-xs lg:text-sm italic">commented</span>
        <p className="mb-1 text-xs lg:text-sm line-clamp-1">
          {props.user.id}
        </p>
        <div className="inline-flex items-center font-semibold text-blue-600 hover:underline text-2xs lg:text-xs">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="12"
            height="12"
            fill="currentColor"
            className="bi bi-chat-right-dots-fill mr-1"
            viewBox="0 0 16 16"
          >
            <path d="M16 2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h9.586a1 1 0 0 1 .707.293l2.853 2.853a.5.5 0 0 0 .854-.353V2zM5 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
          </svg>{" "}
          2 seconds ago
        </div>
      </div>
    </div>
  );
}
