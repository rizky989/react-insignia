import Card from "./Card";
import SmallCard from "./SmallCard";
import UploadCard from "./UploadCard";

export default function HomeCategory(props) {
  return (
    <div>
      {/* Mobile */}
      <div className="block lg:hidden">
        <div className="flex justify-between mb-2">
          <h1 className="text-md">{props.title}</h1>
          <div className="text-xs mt-auto flex">
            Browse all {props.title.toLowerCase()}{" "}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-arrow-right-short"
              viewBox="0 0 16 16"
            >
              <path
                fillRule="evenodd"
                d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"
              />
            </svg>
          </div>
        </div>
        <div className="overflow-x-auto whitespace-nowrap bg-gray-100 ">
          <div className="flex space-x-4">
            {props.data &&
              props.data.map((item2) => {
                return (
                  <div className="w-64" key={item2}>
                    <Card data={item2}/>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
      {/* Desktop */}
      <div className="hidden lg:flex gap-3">
        <div className="w-8/12 flex flex-col gap-3">
          <div className="flex justify-between">
            <h1 className="text-2xl">{props.title}</h1>
            <div className="text-sm mt-auto">
              Browse all {props.title.toLowerCase()}
            </div>
          </div>

          <Card data={props.data[0]}/>
          <div className="flex gap-3">
            <SmallCard data={props.data[1]}/>
            <SmallCard data={props.data[2]}/>
          </div>
        </div>
        <div className="w-4/12 flex flex-col gap-3">
          <h1 className="text-2xl text-transparent">&nbsp; </h1>
          <SmallCard data={props.data[3]}/>
          <SmallCard data={props.data[4]}/>
          <UploadCard />
        </div>
      </div>
      <div className="flex"></div>
    </div>
  );
}
