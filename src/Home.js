import { useQuery } from "@tanstack/react-query";
import ActivityCard from "./components/ActivityCard";
import ChannelCard from "./components/ChannelCard";
import Footer from "./components/Footer";
import HomeCategory from "./components/HomeCategory";
import Searchbar from "./components/Searchbar";

export default function Home() {
  const headers = {
    "app-id": "64ffcfbaef4c30863183696c",
  };

  const initialDummyData = {
    data: [],
    total: 0,
    page: 0,
    limit: 0,
  };

  const getData = (url, queryKey) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const { data } = useQuery({
      queryKey: [queryKey],
      queryFn: () =>
        fetch(url, {
          headers: headers,
        }).then((res) => res.json()),
      initialData: initialDummyData,
    });
    return data;
  };

  const getUser = (category, limit, page) => {
    const queryKey = `user-${category}`;
    const url = `https://dummyapi.io/data/v1/user?limit=${limit}&page=${page}`;
    return getData(url, queryKey);
  };

  const getPost = (category) => {
    const queryKey = `post-${category}`;
    const url = `https://dummyapi.io/data/v1/tag/${category}/post?limit=5`;
    return getData(url, queryKey);
  };

  const userActivity = getUser("activity", 5, 1);
  const userChannel = getUser("channel", 8, 2);
  const postData1 = getPost("water");
  const postData2 = getPost("nature");
  const postData3 = getPost("dog");

  return (
    <div className="flex flex-col gap-6">
      <div className="flex-grow block lg:hidden">
        <Searchbar />
      </div>
      <div className="grid grid-cols-1 lg:grid-cols-3 gap-4">
        <div className="lg:col-span-2 flex flex-col gap-6">
          <HomeCategory data={postData1.data} title="Videos" />
        </div>
        <div className="flex flex-col gap-6">
          <div>
            <div className="lg:border-b-4 pb-3 flex justify-between">
              <span className="text-md lg:text-3xl">Activity</span>
              <span className="text-xs lg:text-sm mt-auto flex">
                View timeline
                <span className="lg:block hidden">/ Filter Activities</span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-arrow-right-short block lg:hidden"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"
                  />
                </svg>
              </span>
            </div>
            <div className="flex flex-col gap-3 mt-3 pb-4 border-b border-primary">
              {userActivity.data.map((item) => {
                return <ActivityCard key={item} user={item} />;
              })}
            </div>
          </div>
        </div>
        <div className="lg:col-span-2 flex flex-col gap-6">
          <HomeCategory data={postData2.data} title="People" />
        </div>
        <div className="flex flex-col gap-6">
          <div>
            <div className="lg:border-b-4 pb-3 flex justify-between">
              <span className="text-md lg:text-3xl">Channels</span>
              <span className="text-xs lg:text-sm mt-auto flex">
                Browse all channels
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-arrow-right-short block lg:hidden"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"
                  />
                </svg>
              </span>
            </div>
            <div className="grid grid-cols-3 lg:grid-cols-2 gap-3 mt-3 pb-4 border-b border-primary">
              {userChannel.data.map((item) => {
                return <ChannelCard data={item} key={item} />;
              })}
            </div>
          </div>
        </div>
        <div className="lg:col-span-2 flex flex-col gap-6">
          <HomeCategory data={postData3.data} title="Documents" />
        </div>
      </div>

      <Footer />
    </div>
  );
}
