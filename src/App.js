import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import Home from "./Home";
import Header from "./components/Header";
import Navbar from "./components/Nav";

const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <main className="bg-customred text-primary">
        <Header />
        <div className="mx-auto min-h-screen max-w-screen-xl px-4">
          <div className="hidden lg:block">
            <Navbar />
          </div>
          <div className="">
            <Home />
          </div>
        </div>
      </main>
    </QueryClientProvider>
  );
}
